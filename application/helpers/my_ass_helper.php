<?php

//admin
    function names()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->userdata('nama');
    }
    
    function is_id()
    {
        // $nama=names();
        $CI = get_instance();
        $CI->load->model('M_admin');
        $get_id=$CI->db->get_where('admin',['username'=>names()])->row_array();
      
        return $get_id['id_admin'];
    }

    function is_id_perusahaan()
    {
        $CI = get_instance();
        $CI->load->library('session');

        return $CI->session->userdata('id_perusahaan');
    }

    function is_id_alumni()
    {
        // $nama=names();
        $CI = get_instance();
        $CI->load->library('session');

        return $CI->session->userdata('id_alumni');
    }

    function is_image_alumni()
    {
        $CI = get_instance();
        $CI->load->model('M_alumni');
        $get_id=$CI->db->get_where('alumni',['id_alumni'=>is_id_alumni()])->row_array();
        
        if($get_id['gambar'] != NULL){
            return $get_id['gambar'];
        }else{
            return "batman.png";
        }     
    }
    // all
    function is_image()
    {
        $CI = get_instance();
        $CI->load->model('M_admin');
        $get_id=$CI->db->get_where('admin',['username'=>names()])->row_array();
        
        if($get_id['gambar'] != NULL){
            return $get_id['gambar'];
        }else{
            return "batman.png";
        }     
    }


    function succes_Add()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_flashdata('Add','Data Telah Di Tambah');
    }
    
    function succes_Update()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_flashdata('Update','Data Telah Di Update ');
    }

    function succes_Delete()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_flashdata('Delete','Data Telah Di Hapus');
    }

