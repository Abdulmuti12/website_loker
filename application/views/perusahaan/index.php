
<style>
	.pdfobject-container { height: 300px;} */
	.pdfobject { border: 1px solid #666; }
</style>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <!-- <button class="btn btn-success" data-toggle="modal" data-target="#myModalAdd">
  				Tambah Data
		</button> -->
    </div>
	
    <div class="card-body">
    	<div class="table-responsive scrollmenu">
			<table id="table-perusahaan" class="mb-0 table " style="width:100%">
				<thead>
				<tr>
				<th>Action</th>
				<th>Nama Perusahaan</th>
				<th>Perwakilan</th>
				<th>Status</th>
				<th>Email</th>
				<th>Telpn</th>
				<th>Alamat</th>
				</tr>
				</thead>
			</table> 
		</div>
	</div>
</div>


<!-- modal add -->
<form id="add-row-form" action="<?php echo base_url('perusahaan/create')?>" method="post">
	     <div class="modal" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                   <h4 class="modal-title" id="myModalLabel">Add New</h4>
	               </div>
				   <div class="modal-body">
 	                   <div class="form-group">
							<!-- <input type="hidden" name="id_perusahaan" class="form-control" placeholder="" required> -->
 	                		<input type="text" name="nama" class="form-control" placeholder="nama" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="perwakilan" class="form-control" placeholder="perwakilan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="email" class="form-control" placeholder="Email" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="alamat" class="form-control" placeholder="Alamat" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="telp" class="form-control" placeholder="No Tlpn" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="keterangan" class="form-control" placeholder="" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="tahun_angkatan" class="form-control" placeholder="Keterangan" required>
 	                   </div>

 	               </div>
	               <div class="modal-footer">
	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                  	<button type="submit" id="add-row" class="btn btn-success">Save</button>
	               </div>
	      			</div>
	        </div>
	     </div>
</form>


<form id="add-row-form" action="<?php echo base_url('perusahaan/update');?>" method="post">
	<div class="modal fade" id="myModal" role="dialog">
 	        <div class="modal-dialog">
 	           <div class="modal-content">
 	               <div class="modal-header">
 	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 	                   <h4 class="modal-title" id="myModalLabel">Update Produk</h4>
 	               </div>
 	               <div class="modal-body">
 	                   <div class="form-group">
							<input type="hidden" name="id_perusahaan" class="form-control" placeholder="" required>
 	                		<input type="text" name="nama_perusahaan" class="form-control" placeholder="nama_perusahaan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="perwakilan_perusahaan" class="form-control" placeholder="perwakilan_perusahaan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="email_perusahaan" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="alamat" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="no_tlpn" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="email" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="tahun_angkatan" class="form-control" placeholder="Keterangan" required>
 	                   </div>

 	               </div>
 	               <div class="modal-footer">
 	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	                  	<button type="submit" id="add-row" class="btn btn-success">Update</button>
 	               </div>
 	      			</div>
 	        </div>
 	     </div>
</form>

<!-- modal_view_pdf -->

<div class="modal fade mb-4" id="view_pdf" role="dialog">
 	        <div class="modal-dialog">
 	           <div class="modal-content">
 	               <div class="modal-header">
 	                   <h4 class="modal-title" id="myModalLabel">Komfirmasi Perusahaan</h4>
 	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close" id='close_pdf'><span aria-hidden="true">&times;</span></button>
 	               </div>
					<input type="hidden" name='id_perusahaan' id='id_perusahaan'>
 	               <div class="modal-body">
						<div id='kosong' style="display:none">
							<h3 class="text-center">Surat Belum Di Upload</h3>
						</div>
					<div id="buka_surat"></div>

 	               </div>
 	               <div class="modal-footer">
 	                   	<button type="button" class="btn btn-danger col text-center button_pdf" data-dismiss="modal">Reject</button>
 	                  	<button type="submit" id="verificated" class="btn btn-success col text-center button_pdf">Accept</button>
 	               </div>
 	      		</div>
 	        </div>
 	</div>
<!-- endview		   -->

<!-- modal preview -->
<div class="modal fade" id="modal_preview">
	<form action="<?php echo base_url('P_data_loker/update')?>" method="post" id="msform">
	<!-- progressbar -->
		<ul id="progressbar">
			<li class="active">Informasi Perusahaan</li>
			<li>Tahap 2</li>
			<li>Tahap 3</li>
			<button type="button" class="close" onclick='step2_to()'>
					<span aria-hidden="true">&times;</span>
			</button>
		</ul>

		<fieldset>
			<h2 class="fs-title">Informasi Perusahaan</h2>
			<h3 class="fs-subtitle1">This is step 1</h3>
			<input class="input" type="text" name="nama_perusahaan" id="nama_perusahaan" placeholder="Posisi" required readonly />
			<input class="input" type="text" name="perwakilan_perusahaan" required readonly />
			<textarea name="keterangan_perusahaan" placeholder="Keterangan Lain" class="text float-left" readonly></textarea>

			<input  type="button" name="next"  class="next action-button" value="Next" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Kontak Perusahaan</h2>
			<h3 class="fs-subtitle">Your presence on the social network</h3>
			<input class="input" type="text" name="telp_perusahaan" readonly/>
			<input class="input" type="text" name="email_perusahaan" placeholder="tugas" readonly />
			<input type="button" name="previous" class="previous action-button"  value="Previous" />
			<input  type="button" name="next" class="next action-button" value="Next" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Alamat Perusahaan</h2>
			<h3 class="fs-subtitle">We will never sell it</h3>
			<textarea name="alamat_perusahaan" placeholder="Keterangan Lain" class="text float-left" readonly></textarea>
			<input  type="button" name="previous" class="previous action-button" value="Previous" readonly />
		</fieldset>
	</form>
</div>

<div class="modal fade docs-example-modal-lg" tabindex="-1" role="dialog" id='modal_loker' aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Data Loker</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			</div>
    		<div class="card-body">
    			<div class="table-responsive scrollmenu">
					<div id="result_loker"></div>
				</div>
			</div>
		</div>	
    </div>
</div>




<!-- modal Hapus -->
 <form id="add-row-form" action="<?php echo base_url('perusahaan/delete')?>" method="post">
	<div class="modal" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Hapus Produk</h4>
				</div>

				<div class="modal-body">
					<input type="hidden" name="id_perusahaan" class="form-control" placeholder="Kode Barang" required>
					<strong>Anda yakin mau menghapus record ini?</strong>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="add-row" class="btn btn-danger">Hapus</button>
				</div>
			</div>
		</div>
	</div>
	</form>
<!-- end modal hapus -->

<script>
	
	function step2_to()
    {
		// $('#myModalAdd').modal('hide');
		$('#msform').hide();
		location.reload();
    }

	// start form
	$(function() {
		//jQuery time
		var current_fs, next_fs, previous_fs; //fieldsets
		var left, opacity, scale; //fieldset properties which we will animate
		var animating; //flag to prevent quick multi-click glitches

		$(".next").click(function(){
			if(animating) return false;
			animating = true;
			
			current_fs = $(this).parent();
			next_fs = $(this).parent().next();
			
			//activate next step on progressbar using the index of next_fs
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
			
			//show the next fieldset
			next_fs.show(); 
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function(now, mx) {
					//as the opacity of current_fs reduces to 0 - stored in "now"
					//1. scale current_fs down to 80%
					scale = 1 - (1 - now) * 0.2;
					//2. bring next_fs from the right(50%)
					left = (now * 50)+"%";
					//3. increase opacity of next_fs to 1 as it moves in
					opacity = 1 - now;
					current_fs.css({'transform': 'scale('+scale+')'});
					next_fs.css({'left': left, 'opacity': opacity});
				}, 
				duration: 800, 
				complete: function(){
					current_fs.hide();
					animating = false;
				}, 
				//this comes from the custom easing plugin
				easing: 'easeInOutBack'
			});
		});

		$(".previous").click(function(){
			if(animating) return false;
			animating = true;
			
			current_fs = $(this).parent();
			previous_fs = $(this).parent().prev();
			
			//de-activate current step on progressbar
			$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
			
			//show the previous fieldset
			previous_fs.show(); 
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function(now, mx) {
					//as the opacity of current_fs reduces to 0 - stored in "now"
					//1. scale previous_fs from 80% to 100%
					scale = 0.8 + (1 - now) * 0.2;
					//2. take current_fs to the right(50%) - from 0%
					left = ((1-now) * 50)+"%";
					//3. increase opacity of previous_fs to 1 as it moves in
					opacity = 1 - now;
					current_fs.css({'left': left});
					previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
				}, 
				duration: 800, 
				complete: function(){
					current_fs.hide();
					animating = false;
				}, 
				//this comes from the custom easing plugin
				easing: 'easeInOutBack'
			});
		});

		
	});
	
	$(document).ready(function(){
		// start datatable
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-perusahaan").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#table-perusahaan input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
				});
			},
				oLanguage: {
				sProcessing: "loading..."
			},
				processing: true,
        		responsive: true,

				serverSide: true,
				ajax: {"url": "<?php echo base_url()?>perusahaan/json_perusahaan", "type": "POST"},
						columns: [
							// {"data": "id_alumni"},
            				{"data": "view"},
							{"data": "nama_perusahaan"},
							// {"data": "nama_perusahaan"},
							{data: {perwakilan_perusahaan : "perwakilan_perusahaan", id_admin : "id_admin"},
									mRender : function(data, type, full) {
                					return '<a  class="bg-white text-center loker" id='+ data.id_perusahaan +'> '+ data.perwakilan_perusahaan +'</a>'
									
              						} 
            				},
							{"data": "status_komfirmasi",

								render: function(data) {
									if(data === 'Waiting Verification') {
										return '<p><a href="#" class="text-white bg-primary">Waiting Comfirm</a></p>'
										// return '<button class="btn btn" id='+data+' onclick="onmy()">Comfirm</button>'
               						}else if(data === 'Rejected'){
										return '<p><span class="text-white bg-danger">Rejected</span></p>'
									}else if(data === 'Verificated'){
										return '<p><a href="#" class="text-white bg-success">Verificated</a></p>'
									}
									else{
										return '<span class="bg-default"  >No Status</button>'
									}
								}
							 },
							{"data": "email_perusahaan"},
							{"data": "telp_perusahaan"},
							{"data": "alamat_perusahaan"}
					],
					order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				let info = this.fnPagingInfo();
				let page = info.iPage;
				let length = info.iLength;
				
				$('td:eq(0)', row).html();
			}
		});
		// enddata_table


		// preview form
		$('#table-perusahaan').on('click','.preview',function(){
			
			// alert(this.id);
			let id = this.id;
			$.ajax({
			url: "<?php echo base_url('Perusahaan/preview'); ?>",
			type: "POST",
			data:{"id":id}
			}).done(function(data) {
				// alert(dok);
				$("#modal_preview").modal();
				let my = data;
				obj = JSON.parse(my);
		 		$('[name="id_perusahaan"]').val( obj.id_perusahaan);
				$('[name="nama_perusahaan"]').val("Nama Perusahaan: " + obj.nama_perusahaan);
				$('[name="perwakilan_perusahaan"]').val("Perwakilan: " + obj.perwakilan_perusahaan);
				$('[name="alamat_perusahaan"]').val("Alamat: " + obj.alamat_perusahaan);
				$('[name="email_perusahaan"]').val("Email : " + obj.email_perusahaan);
				$('[name="telp_perusahaan"]').val("Telephone: " + obj.telp_perusahaan);
				$('[name="keterangan_perusahaan"]').val("Keterangan: " + obj.keterangan_perusahaan);
			});
		});

		// komfirmasi surat
		$('#table-perusahaan').on('click','.look_later',function(){
			let id = this.id;
			$.ajax({
			url: "<?php echo base_url('Perusahaan/view_pdf'); ?>",
			type: "POST",
			data:{"id":id}
			}).done(function(data) {
				// alert(dok);
				$("#view_pdf").modal();
				let my = data;
				obj = JSON.parse(my);
		 		$('[name="id_perusahaan"]').val( obj.id_perusahaan);
					if( obj.status_komfirmasi === 'Verificated' ){
						$("#kosong").hide();
						$("#buka_surat").show();
						//button
						$(".btn-success").hide();
						$(".btn-danger").show();
						PDFObject.embed("<?=base_url().'assets/surat_perusahaan/';?>" + obj.surat, "#buka_surat");
				 	}else if(obj.status_komfirmasi === 'Rejected'){
						$("#kosong").hide();
						$("#buka_surat").show();
						//button
						$(".btn-success").show();
						$(".btn-danger").hide();
						PDFObject.embed("<?=base_url().'assets/surat_perusahaan/';?>" + obj.surat, "#buka_surat");
					}else if(obj.status_komfirmasi ==='Waiting Verification'){
						$("#kosong").hide();
						$("#buka_surat").show();
						PDFObject.embed("<?=base_url().'assets/surat_perusahaan/';?>" + obj.surat, "#buka_surat");
						//button
						$(".btn-success").show();
						$(".btn-danger").show();
					}
					 else{
						$("#buka_surat").hide();
						$("#kosong").show();
						$(".button_pdf").hide();
					 }
			});
		});

// ---------------------------------------------------------komfirmasi perusahaan		
		// action surat Reject
		$('#view_pdf').on('click','.btn-danger ',function(){

			let id= $('#id_perusahaan').val();
			let status= "Rejected";
			postdata={id:id,status:status};
	        $.ajax({
	        method:'POST',
	            url:'<?php echo base_url()?>perusahaan/comfirm',
	            data:postdata,
	            success: function(response) { 
	                // refresh
	                setTimeout(function(){
						$('#table-perusahaan').DataTable().ajax.reload();
	                 }, 1000); 
	            }
	        });  
		});

		// action surat Verificated
		$('#view_pdf').on('click','#verificated ',function(){
			let id= $('#id_perusahaan').val();
			let status= "Verificated";
			postdata={id:id,status:status};
	        $.ajax({
	        method:'POST',
	            url:'<?php echo base_url()?>perusahaan/comfirm',
	            data:postdata,
	            success: function(response) { 

					window.location.reload();

					// 	alert(0);
					// setTimeout(function(){
					// 	$('#table-perusahaan').DataTable().ajax.reload();
	                //  }, 1000); 
	            }
	        });  
		});
// ---------------------------------------------------------end_komfirmasi perusahaan		
	
		// modal_loker
		$('#table-perusahaan').on('click','.loker',function(){
            let id_perusahaan=this.id;
			let url = "<?=site_url();?>Perusahaan/perusahaan_loker";
			var param ={id_perusahaan:id_perusahaan};

			$.ajax({
	        method:'POST',
	            url:url,
	            data:param,
	            success: function(msg) { 
					// load_page
				 $('#modal_loker').modal();
				 $('#result_loker').html(msg);

	            }
	        });  

     	});

		// start_hapus
		$('#table-perusahaan').on('click','.hapus_record',function(){
            let id_perusahaan=this.id;
			// alert(this.id);
            $('#ModalHapus').modal('show');
            $('[name="id_perusahaan"]').val(id_perusahaan);
     	});
			
	});
	</script>