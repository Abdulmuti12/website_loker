<div class="card shadow mb-4">
    <div class="card-header py-3">
        <button class="btn btn-success" data-toggle="modal" data-target="#myModalAdd">
  				Tambah Data
		</button>
    </div>
    <div class="card-body">
    	<div class="table-responsive scrollmenu">
			<table id="table-jurusan" class="mb-0 table" style="width:100%">
				<thead>
				<tr>
				<th>Action</th>
				<th>Jurusan</th>
				<th>Keterangan</th>
				</tr>
				</thead>
			</table> 
		</div>
	</div>
</div>

<!-- modal Hapus -->
<form id="add-row-form" action="<?php echo base_url('jurusan/delete')?>" method="post">
	<div class="modal" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Hapus Produk</h4>
				</div>

				<div class="modal-body">
					<input type="hidden" name="id_jurusan" class="form-control" placeholder="Kode Barang" required>
					<strong>Anda yakin mau menghapus record ini?</strong>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="add-row" class="btn btn-success">Hapus</button>
				</div>
			</div>
		</div>
	</div>
	</form>
<!-- end modal hapus -->
<!-- modal add -->
<form id="add-row-form" action="<?php echo base_url('jurusan/create')?>" method="post">
	     <div class="modal" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                   <h4 class="modal-title" id="myModalLabel">Add New</h4>
	               </div>
	               <div class="modal-body">
	                   <div class="form-group">
	                       <input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required>
	                   </div>
						
						<div class="form-group">
	                       <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" required>
	                   </div>
	               </div>
	               <div class="modal-footer">
	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                  	<button type="submit" id="add-row" class="btn btn-success">Save</button>
	               </div>
	      			</div>
	        </div>
	     </div>
</form>

<form id="add-row-form" action="<?php echo base_url('jurusan/update');?>" method="post">
	<div class="modal fade" id="myModal" role="dialog">
 	        <div class="modal-dialog">
 	           <div class="modal-content">
 	               <div class="modal-header">
 	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 	                   <h4 class="modal-title" id="myModalLabel">Update Produk</h4>
 	               </div>
 	               <div class="modal-body">
 	                   <div class="form-group">
							<input type="hidden" name="id_jurusan" class="form-control" placeholder="" required>
 	                		<input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 	               </div>
 	               <div class="modal-footer">
 	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	                  	<button type="submit" id="add-row" class="btn btn-success">Update</button>
 	               </div>
 	      			</div>
 	        </div>
 	     </div>
</form>

<script>
	$(document).ready(function(){
		// start datatable
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-jurusan").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
				});
			},
				oLanguage: {
				sProcessing: "loading..."
			},
				processing: true,
				serverSide: true,
				ajax: {"url": "<?php echo base_url()?>jurusan/json_jurusan", "type": "POST"},
					columns: [
						// {"data": "id_jurusan"},
						{"data": "view"},
						{"data": "jurusan"},
						{"data": "keterangan"}
					],
					order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				$('td:eq(0)', row).html();
			}
		});
		// enddata_table

		// preview
		$('#table-jurusan').on('click','.preview',function(){

			var id = this.id;
			$.ajax({
			url: "<?php echo base_url('jurusan/edit'); ?>",
			type: "POST",
			data:{"id":id}
			}).done(function(data) {
				// alert(data);
				$("#myModal").modal();
				var my = data;
				obj = JSON.parse(my);
				// console.log();
				$('[name="id_jurusan"]').val(obj.id_jurusan);
				$('[name="jurusan"]').val(obj.jurusan);
				$('[name="keterangan"]').val(obj.keterangan);
				// $('[name="email"]').val(obj.email);
		
			});
		});
		// start_hapus
		$('#table-jurusan').on('click','.hapus_record',function(){
            var id_jurusan=this.id;
			// alert(this.id);
            $('#ModalHapus').modal('show');
            $('[name="id_jurusan"]').val(id_jurusan);
     	});		
	});
	</script>