
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <button class="btn btn-success" data-toggle="modal" data-target="#myModalAdd">
  				Tambah Data
		</button>
    </div>
    <div class="card-body">
    	<div class="table-responsive scrollmenu">
			<table id="table-alumni" class="mb-0 table" style="width:100%">
				<thead>
				<tr>
				<th>Action</th>
				<th>Nama</th>
				<th>NIS</th>
				<th>Alamat</th>
				<th>Tlpn</th>
				<th>email</th>
				</tr>
				</thead>
			</table> 
		</div>
	</div>
</div>

<!-- modal Hapus -->
<form id="add-row-form" action="<?php echo base_url('alumni/delete')?>" method="post">
	<div class="modal" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Hapus Produk</h4>
				</div>

				<div class="modal-body">
					<input type="hidden" name="id_alumni" class="form-control" placeholder="Kode Barang" required>
					<strong>Anda yakin mau menghapus record ini?</strong>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="add-row" class="btn btn-success">Hapus</button>
				</div>
			</div>
		</div>
	</div>
	</form>
<!-- end modal hapus -->
<!-- modal add -->
<form id="add-row-form" action="<?php echo base_url('alumni/create')?>" method="post">
	     <div class="modal" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                   <h4 class="modal-title" id="myModalLabel">Add New</h4>
	               </div>
	               <div class="modal-body">
	                   <div class="form-group">
	                       <input type="text" name="nama_lengkap" class="form-control" placeholder="nama_lengkap" required>
	                   </div>
						
						<div class="form-group">
	                       <input type="text" name="nis" class="form-control" placeholder="nis" required>
	                   </div>
						<div class="form-group">
	                       <input type="text" name="jenis_kelamin" class="form-control" placeholder="jenis_kelamin" required>
					   </div>
					   	<div class="form-group">
						 	<select name="id_jurusan" class="form-control" placeholder="Kode Barang" required>
								<?php foreach ($jurusan->result() as $row) :?>
									<option value="<?php echo $row->id_jurusan;?>"><?php echo $row->jurusan;?></option>
								<?php endforeach;?>
							</select>
					 	</div>
						<div class="form-group">
	                       <input type="text" name="alamat" class="form-control" placeholder="alamat" required>
	                   </div>
						<div class="form-group">
	                       <input type="text" name="email" class="form-control" placeholder="email" required>
	                   </div>
						<div class="form-group">
	                       <input type="text" name="no_tlpn" class="form-control" placeholder="no_tlpn" required>
	                   </div>
						<div class="form-group">
	                       <input type="text" name="tanggal_lahir" class="form-control" placeholder="tanggal_lahir" required>
	                   </div>
						<div class="form-group">
	                       <input type="text" name="tahun_angkatan" class="form-control" placeholder="tahun_angkatan" required>
	                   </div>
	               </div>
	               <div class="modal-footer">
	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                  	<button type="submit" id="add-row" class="btn btn-success">Save</button>
	               </div>
	      			</div>
	        </div>
	     </div>
</form>

<form id="add-row-form" action="<?php echo base_url('alumni/update');?>" method="post">
	<div class="modal fade" id="myModal" role="dialog">
 	        <div class="modal-dialog">
 	           <div class="modal-content">
 	               <div class="modal-header">
 	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 	                   <h4 class="modal-title" id="myModalLabel">Update Produk</h4>
 	               </div>
 	               <div class="modal-body">
 	                   <div class="form-group">
							<input type="hidden" name="id_alumni" class="form-control" placeholder="" required>
 	                		<input type="text" name="nama_lengkap" class="form-control" placeholder="nama_lengkap" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="nis" class="form-control" placeholder="nis" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="alamat" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="no_tlpn" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="email" class="form-control" placeholder="Keterangan" required>
 	                   </div>
 						<div class="form-group">
 	                       <input type="text" name="tahun_angkatan" class="form-control" placeholder="Keterangan" required>
 	                   </div>
						<div class="form-group">
 	                       <select name="id_jurusan" class="form-control" placeholder="Kode Barang" required>
								<?php foreach ($jurusan->result() as $row) :?>
									<option value="<?php echo $row->id_jurusan;?>"><?php echo $row->jurusan;?></option>
								<?php endforeach;?>
 							</select>
 	                   </div>
 	               </div>
 	               <div class="modal-footer">
 	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	                  	<button type="submit" id="add-row" class="btn btn-success">Update</button>
 	               </div>
 	      			</div>
 	        </div>
 	     </div>
</form>

<script>
	$(document).ready(function(){
		// start datatable
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-alumni").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
				});
			},
				oLanguage: {
				sProcessing: "loading..."
			},
				processing: true,
				serverSide: true,
				ajax: {"url": "<?php echo base_url()?>alumni/json_alumni", "type": "POST"},
						columns: [
							// {"data": "id_alumni"},
							{"data": "view"},
							{"data": "nama_lengkap"},
							{"data": "nis"},
							{"data": "alamat"},
							{"data": "no_tlpn"},
							{"data": "email"}
					],
					order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				let info = this.fnPagingInfo();
				let page = info.iPage;
				let length = info.iLength;
				$('td:eq(0)', row).html();
			}
		});
		// enddata_table

		// preview
		$('#table-alumni').on('click','.preview',function(){
			// alert(this.id);
			let id = this.id;
			$.ajax({
			url: "<?php echo base_url('alumni/edit'); ?>",
			type: "POST",
			data:{"id":id}
			}).done(function(data) {
				// alert(dok);
				$("#myModal").modal();
				let my = data;
				obj = JSON.parse(my);
				// console.log();
				$('[name="id_alumni"]').val(obj.id_alumni);
				$('[name="nama_lengkap"]').val(obj.nama_lengkap);
				$('[name="nis"]').val(obj.nis);
				$('[name="alamat"]').val(obj.alamat);
				$('[name="no_tlpn"]').val(obj.no_tlpn);
				$('[name="email"]').val(obj.email);
				$('[name="tahun_angkatan"]').val(obj.tahun_angkatan);
				$('[name="id_jurusan"]').val(obj.id_jurusan);
		
			});
		});
		// start_hapus
		$('#table-alumni').on('click','.hapus_record',function(){
            let id_alumni=this.id;
			// alert(this.id);
            $('#ModalHapus').modal('show');
            $('[name="id_alumni"]').val(id_alumni);
     	});		
	});
	</script>