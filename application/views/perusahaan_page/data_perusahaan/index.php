<!-- <div class="modal " id='profile-menu'> -->
<div class="card shadow mb-4" id='profile-menu'>
    <div class="card-header py-3">
		<?php
			if($status_komfirmasi == ""){

				echo	"Verification Status: Account Need Vertivication";
		        echo "<button class='bnt btn-facebook float-right' onclick='upload_photo()'>  <i class='fas fa-file'></i></button>";

			}
			elseif($status_komfirmasi == "Waiting Verification"){
				echo	"Vertication Status: Waiting Verificationn";
				echo "<button class='bnt btn-facebook float-right'>  <i class='fas fa-file'></i></button>";
			}
			elseif($status_komfirmasi == "Verificated"){
				echo	"Vertication Status: Waiting Verificationn";
				echo "<button class='bnt btn-facebook float-right'>  <i class='fas fa-file'></i></button>";
			}
			else{
				echo	"Vertication Status: Account Need Vertivication";
				echo "<button class='bnt btn-facebook float-right'>  <i class='fas fa-file'></i></button>";
			}
			?>
    </div>
    <div class="card-body">

		<div class="form-group row">
			<label for="staticEmail" class="col-sm-2 col-form-label">Nama Perusahaan</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="staticEmail" value="<?=$nama_perusahaan;?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="staticEmail" class="col-sm-2 col-form-label">Perwakilan</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="staticEmail" value="<?=$perwakilan_perusahaan;?>">
			</div>
		</div>
			

		<div class="form-group row">
			<div class="form-group col-md-6">
				<label for="inputEmail4">Email</label>
				<label class="sr-only" for="inlineFormInputGroupUsername2">Email</label>
				<div class="input-group ">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<i class="material-icons">&#xe0d0;</i>
						</div>
					</div>
					<input type="text" class="form-control" value="<?=$email_perusahaan;?>" placeholder="Email">
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="inputEmail4">No Tlpn</label>
				<label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
				<div class="input-group ">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-phone"></i></div>
					</div>
					<input type="text" class="form-control" value="<?=$telp_perusahaan;?>" placeholder="Username">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="exampleFormControlTextarea1">Example textarea</label>
			<textarea class="form-control" id="exampleFormControlTextarea1" rows="1">
				<?php echo htmlspecialchars($alamat_perusahaan); ?>
			</textarea>
		</div>

		<div class="form-group">
			<label for="exampleFormControlTextarea1">Keterangan Perusahaan</label>
			<textarea class="form-control" id="exampleFormControlTextarea1" rows="2">
				<?php echo htmlspecialchars($keterangan_perusahaan); ?>
			</textarea>
		</div>

		<div class="form-group row">
			<label for="staticEmail" class="col-sm-2 col-form-label">Data Upload</label>
			<div class="col-sm-10">
			<?php
			if($status_komfirmasi == ""){

		        echo "<input type='text' readonly class='form-control' id='staticEmail' value='Anda Belum Mengupload Data '>";

			}
			elseif($status_komfirmasi == "Waiting Verification"){

				echo "<input type='text' readonly class='form-control' id='staticEmail' value='Menunggu Komfirmasi dari admin '>";

			}
			else{
				echo	"Vertication Status: Account Need Vertivication";
				echo "<button class='bnt btn-facebook float-right'>  <i class='fas fa-file'></i></button>";
			}
			?>
			</div>
		</div>

		<div class="form-group">
		
			<div class="form-group col-md-1 float-right">
			<button class="btn btn-warning" onclick="profile_perusahaan()">
			<i class="fas fa-book"></i>
			</button>
			</div>


		</div>

	</div>
</div>

      <!-- <div class="modal-dialog"> -->
<div class="card shadow mb-4" id="Modal-edit-perusahaan" style="display:none">
    <div class="card-header py-3">
		<h3>Data Perusahaan</h3>
    </div>
    <div class="card-body">
		<form id="add-row-form" action="<?php echo base_url('P_data_perusahaan/update');?>" method="post">

		<div class="form-group row">
			<label for="staticEmail" class="col-sm-2 col-form-label">Nama Perusahaan</label>
			<div class="col-sm-10">
				<input type="hidden"  class="form-control" name="id_perusahaan" id="staticEmail" value="<?=$id_perusahaan;?>">
				<input type="text"  class="form-control" name="nama_perusahaan" id="staticEmail" value="<?=$nama_perusahaan;?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="staticEmail" class="col-sm-2 col-form-label">Perwakilan</label>
			<div class="col-sm-10">
				<input type="text"  class="form-control" name="perwakilan_perusahaan" id="staticEmail" value="<?=$perwakilan_perusahaan;?>">
			</div>
		</div>
			

		<div class="form-group row">
			<div class="form-group col-md-6">
				<label for="inputEmail4">Email</label>
				<label class="sr-only" for="inlineFormInputGroupUsername2">Email</label>
				<div class="input-group ">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<i class="material-icons">&#xe0d0;</i>
						</div>
					</div>
					<input type="text" class="form-control" name="email_perusahaan" value="<?=$email_perusahaan;?>" placeholder="Email">
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="inputEmail4">No Tlpn</label>
				<label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
				<div class="input-group ">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-phone"></i></div>
					</div>
					<input type="text" class="form-control" name="telp_perusahaan" value="<?=$telp_perusahaan;?>" placeholder="Username">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="exampleFormControlTextarea1">Example textarea</label>
			<textarea class="form-control" name="alamat_perusahaan" id="exampleFormControlTextarea1" rows="1"><?php echo htmlspecialchars($alamat_perusahaan); ?></textarea>
		</div>

		<div class="form-group">
			<label for="exampleFormControlTextarea1">Keterangan Perusahaan</label>
			<textarea class="form-control" name="keterangan_perusahaan" id="exampleFormControlTextarea1" rows="2">
				<?php echo htmlspecialchars($keterangan_perusahaan); ?>
			</textarea>
		</div>


	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" onclick="back()" >Cancel</button>
		<button type="submit" id="add-row" class="btn btn-success">Update</button>
		</form>
	</div>
</div>

<div class="modal fade" id="Modal_upload" >
    <div class="modal-dialog">
      <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Upload File</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        	<form  class="login100-form validate-form" id="submit">
				<div class="custom-file">
					<input type="file" name="file" class="custom-file-input">
					<label class="custom-file-label" for="customFile">Choose file</label>
				</div>

				<div class="contact100-form-checkbox">
					<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
					<label class="label-checkbox100" for="ckb1">
						Remember me
					</label>
				</div>

				<div class="container-login100-form-btn">
					<input type='submit' class="btn btn-primary btn-lg btn-block" name="Upload" value="Upload">
				</div>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
<!-- </div> -->
<script>
    
	function upload_photo()
    {
      $('#Modal_upload').modal('show');
    }
    
    function profile_perusahaan()
    {
		$("#profile-menu").hide();
		$("#Modal-edit-perusahaan").show();
    }
    function back()
    {
		// alert(33);
		$("#Modal-edit-perusahaan").hide();
		$("#profile-menu").show();
    }
    $(document).ready(function(){

	$('#submit').submit(function(e){
		// alert(33);
	e.preventDefault(); 
			$.ajax({
				url:'<?php echo base_url();?>P_data_perusahaan/do_upload',
				type:"post",
				data:new FormData(this),
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(data){
					alert("Upload Image Berhasil.");
			}
			});
	});
	});	

  </script>