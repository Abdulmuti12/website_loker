<?php if($this->session->flashdata('Add')): ?>
	<div class="alert alert-primary alert-dismissible fade show" role="alert">
  		<strong>Berhasil!</strong> <?= $this->session->flashdata('Add'); ?>
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
  			</button>
	</div>
<?php endif; ?>
<?php if($this->session->flashdata('Update')): ?>
	<div class="alert alert-warning alert-dismissible fade show" role="alert">
  		<strong>Berhasil!</strong> <?= $this->session->flashdata('Update'); ?>
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
  			</button>
	</div>
<?php endif; ?>

<?php if($this->session->flashdata('Delete')): ?>
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
  		<strong>Berhasil!</strong> <?= $this->session->flashdata('Delete'); ?>
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
  			</button>
	</div>
<?php endif; ?>	