<!doctype html>
<html lang="en">
 	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <button class="btn btn-success" data-toggle="modal" data-target="#myModalAdd">
                                    Tambah Data
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <form method='POST' action="<?php echo base_url('secure/remacth_password'); ?>">
                                <input id="password1" name="code" type="text" value="<?php echo @$code;?>" class="form-control input-md" required="">
                                    <label class="col-md-4 control-label" for="piCurrPass">Password</label>
                                    <div class="col-md-4">
                                        <input id="password1" name="password1" type="password" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <label class="col-md-4 control-label" for="piCurrPass">Comfirm Password</label>
                                    <div class="col-md-4">
                                        <input id="password2" name="password2" type="password" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <div class="col-md-4">
                                    <br>
                                    <button class='btn btn-primary'>Confirm</button>
                                    </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </body>
</html>        