<div class="card shadow mb-4">
    <div class="card-header py-3">
        <button class="btn btn-success" data-toggle="modal" data-target="#myModalAdd">
  				Upload Gambar
		</button>
    </div>
    <div class="card-body">
    	<div class="table-responsive">
			 
		</div>
	</div>
</div>

<!-- <form class="form-horizontal" id="submit"> -->
<form class="form-horizontal" id="submit">	     
	<div class="modal" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                   <h4 class="modal-title" id="myModalLabel">Add New</h4>
	               </div>
	               <div class="modal-body">
				
						<div class="form-group">
							<input type="file" name="file">
						</div>

	               </div>
	               <div class="modal-footer">
	                   	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                  	<button type="submit" id="btn_upload" class="btn btn-success">Save</button>
						  <!-- <button type="submit" class="btn btn-success">Submit</button> -->
	               </div>
	      			</div>
	        </div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){

		$('#submit').submit(function(e){
		    e.preventDefault(); 
		         $.ajax({
		             url:'<?php echo base_url();?>admin/do_upload',
		             type:"post",
		             data:new FormData(this),
		             processData:false,
		             contentType:false,
		             cache:false,
		             async:false,
		              success: function(data){
		                  alert("Upload Image Berhasil.");
		           }
		         });
		    });
	});	
</script>
