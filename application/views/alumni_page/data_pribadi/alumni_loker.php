<div class="card shadow mb-4">
    <div class="card-header py-3">

    </div>
    <div class="card-body">
    	<div class="table-responsive scrollmenu">
			<table id="table-loker" class="mb-0 table" style="width:100%">
				<thead>
				<tr>
				<th class='text-center'>Action</th>
				<th>Posisi</th>
				<th>Pendidikan</th>
				<th>Jurusan</th>
				<th>Jenis Kelamin</th>
				<th>Gaji</th>
				<th>Penempatan</th>
				<th>Waktu Bekeja</th>
				<th>Dibuka</th>
				<th>Ditutup</th>
				</tr>
				</thead>
			</table> 
		</div>
	</div>
</div>
<!-- //preview -->
<div class="modal fade" id="modal_preview">
	<form action="<?php echo base_url('P_data_loker/update')?>" method="post" id="msform">
	<!-- progressbar -->
		<ul id="progressbar">
			<li class="active">Tahap 1</li>
			<li>Tahap 2</li>
			<li>Tahap 3</li>
			<button type="button" class="close" onclick='close_tab()'>
				<span aria-hidden="true">&times;</span>
			</button>
		</ul>

		<fieldset>
			<h2 class="fs-title">Informasi Tahap 1</h2>
			<h3 class="fs-subtitle1">This is step 1</h3>
			<input class="input" type="text" name="posisi" id="posisi" placeholder="Posisi" required readonly />
			<input class="input" type="hidden" name="id_loker" id="id_loker" placeholder="Posisi" required readonly />
			<input class="input" type="text" name="gaji" id="posisi" placeholder="gaji" required readonly />
			<h3 class="fs-subtitle">Jenjang Pendidikan</h3>
			<input class="input text-center" type="text"  name="jenjang_pendidikan" readonly />
			<input class="input" type="text"  name="jenis_kelamin" readonly />
			<input class="input" type="text"  name="jurusan" readonly />
			<input  type="button" name="next"  class="next action-button" value="Next" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Informasi Tahap 2</h2>
			<h3 class="fs-subtitle">Your presence on the social network</h3>
			<input class="input" type="text" name="pengalaman" id="" placeholder="Pengalaman" />
			<input class="input" type="text" name="tugas" placeholder="tugas" />
			<input class="input" type="text" name="jaminan" placeholder="jaminan" />
			<input class="input" type="text" name="fasilitas" placeholder="fasilitas" />
			<input type="button" name="previous" class="previous action-button" value="Previous" />
			<input  type="button" name="next" class="next action-button" value="Next" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Informasi Tahap 3</h2>
			<h3 class="fs-subtitle">We will never sell it</h3>
			<input class="input" type="text" name="penempatan" placeholder="penempatan" />
			<input class="input" type="text" name="waktu_bekerja" placeholder="Waktu Bekerja" />
			<input class="input" type="text" name="dibuka" placeholder="dibuka" />
			<input class="input" type="text" name="ditutup" placeholder="ditutup" />
			<textarea name="lainya" placeholder="Keterangan Lain"></textarea>
			<input  type="button" name="previous" class="previous action-button" value="Previous" />
		</fieldset>
	</form>
</div>
<!-- end preview -->
<!-- modal Hapus -->
<form id="add-row-form" action="<?php echo base_url('Loker/delete')?>" method="post">
	<div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Hapus Produk</h4>
				</div>

				<div class="modal-body">
					<input type="hidden" name="id_loker" class="form-control" placeholder="Kode Barang" required>
					<strong>Anda yakin mau menghapus record ini?</strong>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="add-row" class="btn btn-success">Hapus</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- end modal hapus -->

<script>
    //close_form_preview
	function close_tab()
    {
		$('#modal_preview').modal('hide');
		$('#msform').hide();
		location.reload();
    }
		//form_preview
	$(function() {
		//jQuery time
		var current_fs, next_fs, previous_fs; //fieldsets
		var left, opacity, scale; //fieldset properties which we will animate
		var animating; //flag to prevent quick multi-click glitches

		$(".next").click(function(){
			if(animating) return false;
			animating = true;
			
			current_fs = $(this).parent();
			next_fs = $(this).parent().next();
			
			//activate next step on progressbar using the index of next_fs
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
			
			//show the next fieldset
			next_fs.show(); 
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function(now, mx) {
					//as the opacity of current_fs reduces to 0 - stored in "now"
					//1. scale current_fs down to 80%
					scale = 1 - (1 - now) * 0.2;
					//2. bring next_fs from the right(50%)
					left = (now * 50)+"%";
					//3. increase opacity of next_fs to 1 as it moves in
					opacity = 1 - now;
					current_fs.css({'transform': 'scale('+scale+')'});
					next_fs.css({'left': left, 'opacity': opacity});
				}, 
				duration: 800, 
				complete: function(){
					current_fs.hide();
					animating = false;
				}, 
				//this comes from the custom easing plugin
				easing: 'easeInOutBack'
			});
		});

		$(".previous").click(function(){
			if(animating) return false;
			animating = true;
			
			current_fs = $(this).parent();
			previous_fs = $(this).parent().prev();
			
			//de-activate current step on progressbar
			$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
			
			//show the previous fieldset
			previous_fs.show(); 
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function(now, mx) {
					//as the opacity of current_fs reduces to 0 - stored in "now"
					//1. scale previous_fs from 80% to 100%
					scale = 0.8 + (1 - now) * 0.2;
					//2. take current_fs to the right(50%) - from 0%
					left = ((1-now) * 50)+"%";
					//3. increase opacity of previous_fs to 1 as it moves in
					opacity = 1 - now;
					current_fs.css({'left': left});
					previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
				}, 
				duration: 800, 
				complete: function(){
					current_fs.hide();
					animating = false;
				}, 
				//this comes from the custom easing plugin
				easing: 'easeInOutBack'
			});
		});
	});
	// end_form
    $(document).ready(function(){
        		// start datatable
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-loker").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
				});
			},
				oLanguage: {
				sProcessing: "loading..."
			},
				processing: true,
				serverSide: true,
				ajax: {"url": "<?php echo base_url()?>Loker/json_loker_alumni", "type": "POST"},
					columns: [
						// {"data": "id_loker"},
						{"data": "view"},
						{"data": "posisi"},
						{"data": "jenjang_pendidikan"},
						{"data": "jurusan"},
						{"data": "jenis_kelamin"},
						{"data": "gaji"},
						{"data": "penempatan"},
						{"data": "waktu_bekerja"},
						{"data": "ditutup"},
						{"data": "dibuka"}
					],
					order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				let info = this.fnPagingInfo();
				let page = info.iPage;
				let length = info.iLength;
				$('td:eq(0)', row).html();
			}
		});

		$('#table-loker').on('click','.preview',function(){
				
			let id = this.id;
			$.ajax({
			url: "<?php echo base_url('P_data_loker/edit'); ?>",
			type: "POST",
			data:{"id":id}
			}).done(function(data) {
				// alert(dok);
				$("#modal_preview").modal();
				let my = data;
				obj = JSON.parse(my);
				var jenjang_pendidikans1 = obj.jenjang_pendidikan.split(",");
				$('[name="id_loker"]').val(obj.id_loker);
				$('[name="posisi"]').val("Posisi:" + obj.posisi);
				$('[name="gaji"]').val("Gaji: Rp." + obj.gaji);
				$('[name="jenis_kelamin"]').val("Jenis Kelamin: " + obj.jenis_kelamin);
				$('[name="jenjang_pendidikan"]').val(obj.jenjang_pendidikan);
				$('[name="jurusan"]').val(obj.jurusan);
				$('[name="fasilitas"]').val(obj.fasilitas);
				$('[name="jurusan"]').val("Jurusan: " + obj.jurusan);
				$('[name="penempatan"]').val("Penempatan: " + obj.penempatan);
				$('[name="jaminan"]').val("Jaminan: " + obj.jaminan);
				$('[name="tugas"]').val("Tugas : " + obj.tugas);
				$('[name="waktu_bekerja"]').val("Waktu Berkerja: " + obj.waktu_bekerja);
				$('[name="dibuka"]').val("Dibuka: " + obj.dibuka);
				$('[name="ditutup"]').val("Ditutup: " + obj.ditutup);
				$('[name="lainya"]').val("Keterangan Lain: " + obj.lainya);
			});
		});
		// start_hapus
		$('#table-loker').on('click','.hapus_record',function(){
        	let id_loker=this.id;
			// alert(this.id);
            $('#ModalHapus').modal('show');
            $('[name="id_loker"]').val(id_loker);
     	});	
    });    
    </script>    