<form id="upload" >
  <div class='form-group'>
    <div class="custom-file">
      <input type="file" name="cv"  class="custom-file-input" id="customFileLang" lang="in">
      <label class="custom-file-label" for="customFileLang">CV</label>
    </div>
  </div>

  <div class='form-group'>
      <div class="custom-file">
      <input type="file" name="ijasah" class="custom-file-input" id="customFileLang" lang="pl-Pl">
      <label class="custom-file-label" for="customFileLang">Ijasah</label>
      </div>
  </div>

  <div class='form-group'>
      <div class="custom-file">
      <input type="file" name="transkip_nilai" class="custom-file-input" id="customFileLang" lang="pl-Pl">
      <label class="custom-file-label" for="customFileLang">Transkip Nilai</label>
      </div>
  </div>

  <div class='form-group'>
      <div class="custom-file">
      <input type="file" name="lain2" class="custom-file-input" id="customFileLang" lang="pl-Pl">
      <label class="custom-file-label" for="customFileLang">Lainya</label>
      </div>
  </div>

  <div class="form-group">
      <buttton class="btn btn-secondary cancel" onclick="cancel()">Cancel</buttton>
      <!-- <buttton class="btn btn-info save">Upload</buttton> -->
      <input type='submit' class="btn btn-info save" name="Upload" value="Upload">

  </div>
      
</form>

<script>
      $(document).ready(function(){
       
       
        $('#upload').submit(function(e){
        // alert(33);
          e.preventDefault(); 
            $.ajax({
              url:'<?php echo base_url();?>Kelengkapan_data/upload',
              type:"post",
              data:new FormData(this),
              processData:false,
              contentType:false,
              cache:false,
              async:false,
              success: function(data){
                alert(data);
            }
            });
        });

    });    
</script>