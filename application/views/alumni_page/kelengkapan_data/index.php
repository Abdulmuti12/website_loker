<div class="card shadow mb-4">
    <div class="card-header py-3">
    Kelengkapan Data  
    </div>
    <div class="card-body" id='kelengkapan_data'>
    	<!-- <div class="table-responsive scrollmenu"> -->
            <div class="col text-center">
                <!-- this text -->
                <P><?php echo $text; ?></P>
                <?php echo $verifikasi; ?>

                <!-- this button -->
                <div id="form_upload"></div>

            </div>
		<!-- </div> -->
	</div>
</div>

<script>

    function cancel(){
        alert();
        $('#form_upload').hide();
        $('.upload').show();
        $('p').show();
    }

    $(document).ready(function(){

        let verti = "<?php Print($verifikasi); ?>";
        // mengecek vertifikasi telah diisi
        if (verti == '') {
            	$.ajax({
	            method:'POST',
	            url:"<?=site_url();?>Kelengkapan_data/load_resume",
	            success: function(msg) { 
                        // load_page
                    $('.upload').hide();
                    $('p').hide();
                    $('#form_upload').html(msg);
                    $('#form_upload').show();
	            }
	        });  
        }         
        $('#kelengkapan_data').on('click','.upload',function(){
            let id_perusahaan=this.id;
			let url = "<?=site_url();?>Kelengkapan_data/load_form_upload";
			$.ajax({
	        method:'POST',
	            url:url,
	            success: function(msg) { 
                        // load_page
                    $('.upload').hide();
                    $('p').hide();
                    $('#form_upload').html(msg);
                    $('#form_upload').show();
	            }
	        });  
     	});    
    });    
</script>