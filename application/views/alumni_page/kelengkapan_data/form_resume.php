<style>
	.pdfobject-container { height: 300px;} */
	.pdfobject { border: 1px solid #666; }
</style>
<!-- //stage1 -->
<div class="row">
  <div class="col-sm-6">
    <div class="card" style="width:300px">
      <div class="col text-center">
        <br>
        <img class="card-img-circle" src="<?=base_url().'assets/icon/cv.jpeg';?>" alt="Card image" style="width:70%" height='75px'>
      </div>
      <div class="card-body">
        <h4 class="card-title">CV</h4>
        <a href="#" class="btn btn-primary cv" >See </a>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="card" style="width:300px">
    <div class="col text-center">
<br>
      <img class="card-img-circle" src="<?=base_url().'assets/icon/ijasah.png';?>" alt="Card image" style="width:70%" height='75px'>
      </div>      <div class="card-body">
        <h4 class="card-title">Ijasah</h4>
        <a href="#" class="btn btn-primary upload_ijasah"   id='<?=base_url();?>'>See</a>
      </div>
    </div>
  </div>
</div>

<div class='row'>

  <div class="col-sm-6">
    <div class="card" style="width:300px">
    <div class="col text-center">
        <br>
        <img class="card-img-circle" src="<?=base_url().'assets/icon/nilai.png';?>" alt="Card image" style="width:70%" height='75px'>
      </div>      
      <div class="card-body">
        <h4 class="card-title">Transkip Nilai</h4>
        <a href="#" class="btn btn-primary transkip_nilai"   id='<?=base_url();?>'>See</a>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="card" style="width:300px">
    <div class="col text-center">
        <br>
        <img class="card-img-circle" src="<?=base_url().'assets/icon/sertifikat.png';?>" alt="Card image" style="width:70%" height='75px'>
      </div>            <div class="card-body">
        <h4 class="card-title">Lainya</h4>
        <a href="#" class="btn btn-primary lain2" >See</a>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="Modal_upload" >
    <div class="modal-dialog">
      <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Upload File</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        	<form  class="login100-form validate-form" id="upload_edit">
				<div class="custom-file">
					<input type="file" name="file" class="custom-file-input">
					<label class="custom-file-label" for="customFile">Choose file</label>
				</div>
				<div class="form-group">
        <input type="text" name="field" class="">
        <input type="text" name="nama_sblm" class="">
				</div>


				<div class="contact100-form-checkbox">
					<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
					<label class="label-checkbox100" for="ckb1">
						Remember me
					</label>
				</div>

				<div class="container-login100-form-btn">
					<input type='submit' class="btn btn-primary btn-lg btn-block" name="Upload" value="Upload">
				</div>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>
<!-- //modal view data -->
<div class="modal fade" id="modal_view" >
    <div class="modal-dialog">
      <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">View Data</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        	<form  class="login100-form validate-form">
            <div id="view_data"></div>
           </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <div class="col text-center">
            <button class='btn btn-warning edit_view'>Edit File</button>
            <button class='btn btn-danger hapus_view'>Hapus File</button>
          </div> 
        </div>
      </div>
    </div>
</div>

<script>
  // onclick firt class
  $(document).ready(function(){
    
    $(".cv").click(function() {

      let field='cv';
      let CV ="<?=$resume1['cv'];?>";

      if(CV == ''){
        $('#Modal_upload').modal('show');
        $('[name="field"]').val(field);
        // call function send_to
        sent_to(field);
      }else{
        $('#modal_view').modal('show');
        alert(CV);
        PDFObject.embed("<?=base_url().'assets/data_alumni/';?>" + CV, "#view_data");
        $('[name="field"]').val(field);
        $('[name="nama_sblm"]').val(CV);
        sent_to(field);

      }
    });
 
    $(".upload_ijasah").click(function() {

      let field='ijasah';
      let ijasah ='<?=$resume1['ijasah'];?>';

      if(ijasah == ''){
        
        $('#Modal_upload').modal('show');
        $('[name="field"]').val(field);

        sent_to(field);
      }else{
       
        $('#modal_view').modal('show');
        alert(ijasah);
        PDFObject.embed("<?=base_url().'assets/data_alumni/';?>" + ijasah, "#view_data");
        
        $('[name="field"]').val(field);

      }

    });
 
    $(".transkip_nilai").click(function() {

      let field='transkip_nilai';
      let transkip_nilai='<?=$resume1['transkip_nilai'];?>';

      if(transkip_nilai == ''){
      
        $('#Modal_upload').modal('show');
        $('[name="field"]').val(field);

        sent_to(field);
      }else{
        $('#modal_view').modal('show');
        PDFObject.embed("<?=base_url().'assets/data_alumni/';?>" + transkip_nilai, "#view_data");
        $('[name="field"]').val(field);
      }

    });
 
    $(".lain2").click(function() {
      
      let field='lain2';
      let lain2='<?=$resume1['lain2'];?>';
      if(lain2 == ''){
        $('#Modal_upload').modal('show');
        $('[name="field"]').val(field);
        sent_to(field);
      }else{
        $('#modal_view').modal('show');
        PDFObject.embed("<?=base_url().'assets/data_alumni/';?>" + lain2, "#view_data");
        $('[name="field"]').val(field);
      }

    });

    $(".edit_view").click(function() {

      $('#modal_view').modal('hide');
      $('#Modal_upload').modal('show');
    });

    function sent_to(field){
      $('#upload_edit').submit(function(e){

          e.preventDefault(); 
            $.ajax({
              url:'<?=base_url('Kelengkapan_data/upload_edit')?>',
              type:"post",
              data:new FormData(this),
              processData:false,
              contentType:false,
              cache:false,
              async:false,
              success: function(data){
                // alert(data);
                setTimeout(function(){
                  window.location.reload();
	              }, 1000); 
              }
            });
      });
    }
  });
</script>