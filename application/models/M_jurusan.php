<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class M_jurusan extends CI_Model {

	public function getAllUser(){
		return $this->db->get('jurusan')->result_array();
    }
    function user_list(){
		$hasil=$this->db->query("SELECT username,email,nama FROM jurusan");
		return $hasil->result();
	}
	function get_all_jurusan() {
        $this->datatables->select('id_jurusan,jurusan,keterangan');
        $this->datatables->from('jurusan');
		// $this->datatables->order_by('id_jurusan asc');
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="edit_record btn1" id="$1">Edit</a>  <a href="javascript:void(0);" class="hapus_record btn1" id="$1">Hapus</a>
		<a href="javascript:void(0);" class="preview btn1" id="$1">Previewe</a>',
		'id_jurusan');

		return $this->datatables->generate();
		// echo $this->db->last_query();
		// // var_dump($ac);
		// exit();
  }
}