<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class M_loker extends CI_Model {

	function get_jurusan(){
        $jurusan=$this->db->get('jurusan');
        return $jurusan;
    }
	function get_status(){

		$id_perusahaan= is_id_perusahaan();
		$this->db->select('id_perusahaan,status_komfirmasi');
		$this->db->where("perusahaan.id_perusahaan ='$id_perusahaan' ");
		$perusahaan=$this->db->get('perusahaan');
		
        return $perusahaan;
    }
    
	function get_perusahaan_loker() {

		$id_perusahaan= is_id_perusahaan();
		
        $this->datatables->select('id_loker, posisi, gaji, jenjang_pendidikan, jurusan.jurusan, pengalaman, fasilitas, tugas, jaminan, jenis_kelamin, penempatan, waktu_bekerja, dibuka, ditutup,perwakilan_perusahaan, loker.id_perusahaan');
		$this->datatables->from('loker');
		$this->datatables->join('perusahaan', 'perusahaan.id_perusahaan=loker.id_perusahaan');
		$this->datatables->join('jurusan', 'jurusan.id_jurusan=loker.jurusan');
		$this->datatables->where("perusahaan.id_perusahaan ='$id_perusahaan' ");
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="preview btn1" id="$1" ><i class="fa fa-bars"></i></a>
		<a href="javascript:void(0);" class="edit_record btn1" id="$1" ><i class="fa fa-pencil"></i></a> 
		 <a href="javascript:void(0);" class="hapus_record btn1 btn-xs" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		',
		'id_loker');
		echo $this->datatables->generate();
			
	 }

	function get_admin_loker() {
		

        $this->datatables->select('id_loker, posisi, gaji, jenjang_pendidikan, jurusan.jurusan, pengalaman, fasilitas, tugas, jaminan, jenis_kelamin, penempatan, waktu_bekerja, dibuka, ditutup,perwakilan_perusahaan, loker.id_perusahaan');
		$this->datatables->from('loker');
		$this->datatables->join('perusahaan', 'perusahaan.id_perusahaan=loker.id_perusahaan');
		$this->datatables->join('jurusan', 'jurusan.id_jurusan=loker.jurusan');
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="preview btn1" id="$1" ><i class="fa fa-bars"></i></a>
		 <a href="javascript:void(0);" class="hapus_record btn1 btn-xs" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		',
		'id_loker');
		echo $this->datatables->generate();

	}

	function get_alumni_loker() {
		

        $this->datatables->select('id_loker, posisi, gaji, jenjang_pendidikan, jurusan.jurusan, pengalaman, fasilitas, tugas, jaminan, jenis_kelamin, penempatan, waktu_bekerja, dibuka, ditutup,perwakilan_perusahaan, loker.id_perusahaan');
		$this->datatables->from('loker');
		$this->datatables->join('perusahaan', 'perusahaan.id_perusahaan=loker.id_perusahaan');
		$this->datatables->join('jurusan', 'jurusan.id_jurusan=loker.jurusan');
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="preview btn1" id="$1" ><i class="fa fa-bars"></i></a>
		<a href="javascript:void(0);" class="preview btn1" id="$1" ><i class="fa fa-book"></i></a>
		','id_loker');
		echo $this->datatables->generate();
			
	 }
  
}