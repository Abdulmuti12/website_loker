<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_secure extends CI_Model{

	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	
	}	

	function alumn_login($table,$where){

		$this->db->select('id_alumni,nama_lengkap');
		return $this->db->get_where($table,$where);
	}	

	function perusahaan_login($table,$where){

		$this->db->select('id_perusahaan,perwakilan_perusahaan');
		return $this->db->get_where($table,$where);
	}	

}