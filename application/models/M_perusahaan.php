
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_perusahaan extends CI_Model {

    // function get_jurusan(){
    //     $jurusan=$this->db->get('jurusan');
    //     return $jurusan;
    // }


    function get_all_perusahaan() {
        $this->datatables->select('id_perusahaan,nama_perusahaan,email_perusahaan,telp_perusahaan,keterangan_perusahaan,alamat_perusahaan,surat,status_komfirmasi,id_admin,perwakilan_perusahaan');
        $this->datatables->from('perusahaan');
        // $this->datatables->join('jurusan', 'jurusan.id_jurusan=perusahaan.id_jurusan');
		$this->datatables->add_column('view', 
        '<a href="javascript:void(0);" class="preview btn1" id="$1" ><i class="fa fa-bars"></i></a> 
		<a href="javascript:void(0);" class="look_later btn1" id="$1"><i class="fa fa-archive"></i></a>
         <a href="javascript:void(0);" class="hapus_record btn1" id="$1"><i class="fas fa-trash text-gray-300"></i></a>',
		'id_perusahaan');
        return $this->datatables->generate();
        // echo $this->db->last_query();
    }
}