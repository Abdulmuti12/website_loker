<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	public function getAllUser(){
		return $this->db->get('admin')->result_array();
	}
	
    function simpan_img(){
		$hasil=$this->db->query("SELECT username,email,nama FROM admin");
		return $hasil->result();
	}
	
    function user_list(){
		$hasil=$this->db->query("SELECT username,email,nama FROM admin");
		return $hasil->result();
	}

	function get_all_admin() {
        $this->datatables->select('id_admin,status,nama,username,email');
        $this->datatables->from('admin');
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="preview btn1" id="$1"><i class="fa fa-bars"></i></a>
		<a href="javascript:void(0);" class="edit_record btn1" id="$1"  data-nama="$2" data-username="$3" data-email="$4"><i class="fa fa-pencil"></i></a>
		  <a href="javascript:void(0);" class="hapus_record btn1" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		',
		'id_admin,nama,username,email,status');
        return $this->datatables->generate();
 	}
  
}