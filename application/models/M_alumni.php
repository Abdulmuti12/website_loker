
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_alumni extends CI_Model {

    function get_jurusan(){
        $jurusan=$this->db->get('jurusan');
        return $jurusan;
    }

    function cek_resume(){
        $hasil=$this->db->query(" SELECT COUNT(alumni.id_alumni) from alumni INNER JOIN resume ON resume.id_alumni=alumni.id_alumni");
        return $hasil->result();
    }


    function get_all_alumni() {

        $this->datatables->select('id_alumni,nama_lengkap,nis,jenis_kelamin,jurusan,alamat,email,no_tlpn,tanggal_lahir,tahun_angkatan');
        $this->datatables->from('alumni');
        $this->datatables->join('jurusan', 'jurusan.id_jurusan=alumni.id_jurusan');
		$this->datatables->add_column('view', 
        '<a href="javascript:void(0);" class="edit_record btn1" id="$1" ><i class="fa fa-pencil"></i></a>  
        <a href="javascript:void(0);" class="hapus_record btn1" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		<a href="javascript:void(0);" class="preview btn1" id="$1" value="22"><i class="fa fa-bars"></i></a>',
		'id_alumni');
        echo $this->datatables->generate();
        // echo $this->db->last_query();
        // exit;
    }
    
    
}