<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_data_loker extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_loker');
	
	}

	public function index()
    {
		$data1['page_name']='Page Loker';
		$data['jurusan']=$this->M_loker->get_jurusan();
		$query=$this->M_loker->get_status()->row_array();
		
		if($query['status_komfirmasi'] == 'Verificated'){
			
			$data['status']= "<button class='btn btn-success' data-toggle='modal' onclick='step1_to()'>
			Tambah Data</button>";

		}else{
			
			$data['status']= "Anda Belum Dapat Ijin Untuk Menambahkan loker";

		}	

		if($this->session->userdata('nama') != NULL){

            $this->load->view('template/header_perusahaan',$data1);	
			$this->load->view('Template/flashdata');
			$this->load->view('perusahaan_page/loker/index',$data);
			$this->load->view('template/footer');
			
		}
		else{
			redirect(base_url('secure'));
		}
	}

	public function preview_admin()
	{
		$id=$this->input->post('id');
		$data=$this->db->get_where('admin',['id_admin'=>$id])->row_array();
		echo json_encode($data,TRUE);
	}

	function save(){ //function simpan data
		
		$data=array(
		  'posisi'     => $this->input->post('posisi'),
		  'gaji'     => $this->input->post('gaji'),
		  'jenjang_pendidikan' => implode(',',$this->input->post('jenjang_pendidikan')),
		  'pengalaman'    => $this->input->post('pengalaman'),
		  'jurusan'   => $this->input->post('jurusan'),
		  'jenis_kelamin'   => $this->input->post('jenis_kelamin'),
		  'tugas'   => $this->input->post('tugas'),
		  'jaminan'   => $this->input->post('jaminan'),
		  'fasilitas'   => $this->input->post('fasilitas'),
		  'penempatan'   => $this->input->post('penempatan'),
		  'waktu_bekerja'   => $this->input->post('waktu_bekerja'),
		  'dibuka'   => $this->input->post('dibuka'),
		  'ditutup'   => $this->input->post('ditutup'),
		  'lainya'   => $this->input->post('lainya'),
		  'id_perusahaan' => is_id_perusahaan()
		);
		
			// var_dump($data);
			// exit();

		$excute=$this->db->insert('loker', $data);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
		redirect('P_data_loker');
	}

	function update(){ //function update data

		// $id_loker=$this->input->post('id_loker');
		$id_loker=$this->encrypt->decode($this->input->post('id_loker'));

		$data=array(
			'posisi' =>$this->input->post('posisi'),
			'gaji' => $this->input->post('gaji'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'jenjang_pendidikan' => implode(',',$this->input->post('jenjang_pendidikan')),
			'tugas' => $this->input->post('tugas'),
			'jurusan' => $this->input->post('jurusan'),
			'fasilitas' => $this->input->post('fasilitas'),
			'jaminan' => $this->input->post('jaminan'),
			'waktu_bekerja' => $this->input->post('waktu_bekerja'),
			'dibuka' => $this->input->post('dibuka'),
			'penempatan' => $this->input->post('penempatan'),
			'ditutup' => $this->input->post('ditutup'),
			'lainya' => $this->input->post('lainya')
		);
		$this->db->where('id_loker',$id_loker);
		$update=$this->db->update('loker', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('P_data_loker');
	}

	function delete(){ //function hapus data
		$id_loker=$this->input->post('id_loker');
		$this->db->where('id_loker',$id_loker);
		$delete=$this->db->delete('loker');
		if($delete == 1){
			
			succes_Delete();
		}
		else{

		}	  
		redirect('P_data_loker');
	}

	public function edit()
	{
		$id=$this->input->post('id');
		$dok=$this->encrypt->encode($id);
		$this->db->join('jurusan', 'jurusan.id_jurusan = loker.jurusan');
		$this->db->select('id_loker,jurusan.jurusan as no,loker.jurusan as jurusan,posisi, gaji, jenjang_pendidikan, loker.jurusan, pengalaman, fasilitas, tugas, jaminan, jenis_kelamin, penempatan, waktu_bekerja, dibuka, ditutup,lainya');
		$query=$this->db->get_where('loker',['id_loker'=>$id])->row_array();
			// echo $this->db->last_query();
			// exit();
		$encrypt=$this->encrypt->encode($query['id_loker']);
		        // $ac1=$this->encrypt->decode($ac);
			$data=array(
				'id_loker' => $encrypt,
				'posisi' =>$query['posisi'],
				'gaji' => $query['gaji'],
				'jenis_kelamin' => $query['jenis_kelamin'],
				'jenjang_pendidikan' => $query['jenjang_pendidikan'],
				'jurusan' => $query['jurusan'],
				'jurusan1' => $query['no'],
				'pengalaman' => $query['pengalaman'],
				'tugas' => $query['tugas'],
				'fasilitas' => $query['fasilitas'],
				'jaminan' => $query['jaminan'],
				'waktu_bekerja' => $query['waktu_bekerja'],
				'dibuka' => $query['dibuka'],
				'penempatan' => $query['penempatan'],
				'ditutup' => $query['ditutup'],
				'lainya' => $query['lainya']
			);
			// var_dump($data);
			// exit();
		echo json_encode($data,TRUE);
		// echo json_encode(array($num1, $num2));

	}

	function json_loker()
	{
		header('Content-Type: application/json');
		echo $this->M_loker->get_perusahaan_loker();		
	}
	

	function data_admin()
	{
		$dataa=$this->M_admin->user_list();
		echo json_encode($dataa);
	}

	function detail_admin()
	{
		// $data['page_name']='Detail Admin';
		// $data['status']=array("Aktif","Non Aktif");
		if($this->session->userdata('nama') != NULL){
			
			$this->load->view('template/header');
			$this->load->view('Template/flashdata');
			$this->load->view('admin/detail_admin');
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure'));
		}
	}
}