<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pribadi extends CI_Controller {

	public function __construck(){
		
		parent::__construck();
		$this->load->model('M_alumni');
		// $this->load->model('M_loker');
    }
    
	public function index(){
		

        $this->db->select('id_alumni,nama_lengkap, nis, jenis_kelamin, id_jurusan, alamat, email, no_tlpn, tanggal_lahir, gambar, tahun_angkatan, id_admin');
        $id= is_id_alumni();
        $where=" id_alumni='$id' ";
		$data=$this->db->get_where('alumni',$where)->row_array();

		if($this->session->userdata('nama') != NULL){
            $this->load->view('template/header_alumni');
            $this->load->view('alumni_page/data_pribadi/index',$data);
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure'));
		}
	}

	public function alumni_loker()
    {
		if($this->session->userdata('nama') != NULL){
			$this->load->view('Template/header_alumni');
			$this->load->view('Template/flashdata');
			$this->load->view('alumni_page/data_pribadi/alumni_loker');
			$this->load->view('Template/footer');
		}else{
			redirect('secure');
		}
	}

	function json_loker()
	{
		header('Content-Type: application/json');
		echo $this->M_loker->get_admin_loker();		
	}


	function do_upload(){
	
        $config['upload_path']="./assets/images";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
		$this->load->library('upload',$config);
		
	    if($this->upload->do_upload("file")){
			
			$data = array('upload_data' => $this->upload->data());
			$id_alumni= is_id_alumni();
			$image= $data['upload_data']['file_name']; 

			$gambar =["gambar" => $image];

			$this->db->where('id_alumni',$id_alumni);
			$update=$this->db->update('alumni',$gambar);

			echo json_decode($update);
			
        }else{

			$result='gagal';
			echo json_decode($result);
		}
	}
    
}
