<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

	public function __construck()
	{
		parent::__construck();
		$this->load->model('M_admin');
		// $this->load->library('datatables');
		// $this->load->model('Model_Brand');		
	}

	public function index()
	{
		if($this->session->userdata('nama') != NULL){
			$this->load->view('template/header');
			// $this->load->view('template/content');
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure'));
		}
	}
}
