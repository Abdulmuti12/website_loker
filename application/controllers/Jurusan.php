<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller {

    function __construct(){

		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_jurusan');
    }
    
    public function index()
    {
		if($this->session->userdata('nama') != NULL){
			$this->load->view('Template/header');
			$this->load->view('Template/flashdata');
			$this->load->view('jurusan/index');
			$this->load->view('Template/footer');
		}else{
			redirect('secure');
		}
    }

	function create(){ //function simpan data
		$data=array(
		  'jurusan'    => $this->input->post('jurusan'),
		  'keterangan' => $this->input->post('keterangan')
		);
		$excute=$this->db->insert('jurusan', $data);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
		redirect('jurusan/index');
	}
	public function edit()
	{
		$id=$this->input->post('id');
		$data=$this->db->get_where('jurusan',['id_jurusan'=>$id])->row_array();
		echo json_encode($data,TRUE);
	}

	function update(){ //function update data

		$id_jurusan=$this->input->post('id_jurusan');
		$data=array(
			'jurusan' => $this->input->post('jurusan'),
			'keterangan' => $this->input->post('keterangan')
		  );	
		$this->db->where('id_jurusan',$id_jurusan);
		$update=$this->db->update('jurusan', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('jurusan');
	}
    
    function delete(){ //function hapus data
		$id_jurusan=$this->input->post('id_jurusan');
		$this->db->where('id_jurusan',$id_jurusan);
		$delete=$this->db->delete('jurusan');
		if($delete == 1){

			succes_Delete();
			
		}else{

		}
		redirect('jurusan');
	}

    function json_jurusan()
	{
		header('Content-Type: application/json');
		// echo $this->M_jurusan->get_all_jurusan();
		$this->datatables->select('id_jurusan,jurusan,keterangan');
        $this->datatables->from('jurusan');
		// $this->datatables->order_by('id_jurusan asc');
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="edit_record btn1" id="$1"><i class="fa fa-pencil"></i></a>  
		<a href="javascript:void(0);" class="hapus_record btn1" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		<a href="javascript:void(0);" class="preview btn1" id="$1"><i class="fa fa-bars"></i></a>',
		'id_jurusan');

		echo $this->datatables->generate();
	}
}