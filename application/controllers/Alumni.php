<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni extends CI_Controller {

    function __construct(){

		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_alumni');
    }
    
    public function index()
    {
		$x['jurusan']=$this->M_alumni->get_jurusan();
		if($this->session->userdata('nama') != NULL){
		
		$this->load->view('Template/header');
        $this->load->view('Template/flashdata');
        $this->load->view('alumni/index',$x);
		$this->load->view('Template/footer');
		
		}else{
			redirect(base_url('secure'));
		}
    }

	function create(){ //function simpan data
		$data=array(
		  'id_alumni' => uniqid(),
		  'nama_lengkap' => $this->input->post('nama_lengkap'),
		  'nis' => $this->input->post('nis'),
		  'jenis_kelamin'=> $this->input->post('jenis_kelamin'),
		  'alamat'=> $this->input->post('alamat'),
		  'id_jurusan'=> $this->input->post('id_jurusan'),
		  'email'=> $this->input->post('email'),
		  'no_tlpn'=> $this->input->post('no_tlpn'),
		  'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
		  'tahun_angkatan'=> $this->input->post('tahun_angkatan')
		);
		// var_dump($data);
		// exit();	
		$excute=$this->db->insert('alumni', $data);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
		redirect('alumni/index');
	}
	public function edit()
	{
		$id=$this->input->post('id');
		$dok=$this->encrypt->encode($id);
		$query=$this->db->get_where('alumni',['id_alumni'=>$id])->row_array();
		$encrypt=$this->encrypt->encode($query['id_alumni']);
		        // $ac1=$this->encrypt->decode($ac);


				$data=array(
					'id_alumni' => $encrypt,
					'nama_lengkap' =>$query['nama_lengkap'],
					'nis' => $query['nis'],
					'jenis_kelamin'=> $query['jenis_kelamin'],
					'alamat'=> $query['alamat'],
					'id_jurusan'=> $query['id_jurusan'],
					'email'=> $query['email'],
					'no_tlpn'=> $query['no_tlpn'],
					'tanggal_lahir'=> $query['tanggal_lahir'],
					'tahun_angkatan'=> $query['tahun_angkatan']
				  );
			// var_dump($data);
			// exit();
		echo json_encode($data,TRUE);
		// echo json_encode(array($num1, $num2));

	}

	function update(){ //function update data

		$id_alumni=$this->input->post('id_alumni');
		 $desc=$this->encrypt->decode($id_alumni);
		$data=array(
			'nama_lengkap' => $this->input->post('nama_lengkap'),
			'nis' => $this->input->post('nis'),
			'jenis_kelamin'=> $this->input->post('jenis_kelamin'),
			'alamat'=> $this->input->post('alamat'),
			'id_jurusan'=> $this->input->post('id_jurusan'),
			'email'=> $this->input->post('email'),
			'no_tlpn'=> $this->input->post('no_tlpn'),
			'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
			'tahun_angkatan'=> $this->input->post('tahun_angkatan')
		  );	
		$this->db->where('id_alumni',$desc);
		$update=$this->db->update('alumni', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('alumni');
	}

    function delete(){ //function hapus data
		$id_alumni=$this->input->post('id_alumni');
		$this->db->where('id_alumni',$id_alumni);
		$this->db->delete('alumni');
		redirect('alumni');
	}

    function json_alumni()
	{
		header('Content-Type: application/json');
		echo $this->M_alumni->get_all_alumni();
	}
}