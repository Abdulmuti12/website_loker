<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_perusahaan extends CI_Controller {

	public function __construck()
	{
		parent::__construck();
		$this->load->model('M_admin');	
	}
	public function index()
	{
        if($this->session->userdata('nama') != NULL){
				
            $this->load->view('template/header_perusahaan');
            $this->load->view('template/footer');
        }
        else{
            redirect(base_url('secure/perusahaan_login'));
        }
	}
}

