<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_data_perusahaan extends CI_Controller {
	
    public function __construck()
	{
		parent::__construck();
		$this->load->model('M_admin');	
    }
    
	public function index()
	{
		$data1['page_name']='Company Profile';
        $this->db->select('id_perusahaan, nama_perusahaan, email_perusahaan, telp_perusahaan, keterangan_perusahaan, alamat_perusahaan, surat, status_komfirmasi, id_admin, perwakilan_perusahaan, password_perusahaan');
        $id= is_id_perusahaan();
        $where=" id_perusahaan=$id ";
		$data=$this->db->get_where('perusahaan',$where)->row_array();
		if($this->session->userdata('nama') != NULL){

			$this->load->view('template/header_perusahaan',$data1);
			$this->load->view('Template/flashdata');
            $this->load->view('perusahaan_page/data_perusahaan/index',$data);
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure/home'));
		}
	}

	function update(){ //function update data

		$id_perusahaan=$this->input->post('id_perusahaan');
		$data=array(
		  'nama_perusahaan'=> $this->input->post('nama_perusahaan'),
		  'perwakilan_perusahaan'=> $this->input->post('perwakilan_perusahaan'),
		  'email_perusahaan' => $this->input->post('email_perusahaan'),
		  'alamat_perusahaan' => $this->input->post('alamat_perusahaan'),
		  'telp_perusahaan' => $this->input->post('telp_perusahaan'),
		  'keterangan_perusahaan' => $this->input->post('keterangan_perusahaan')
		);
		$this->db->where('id_perusahaan',$id_perusahaan);
		$update=$this->db->update('perusahaan', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('P_data_perusahaan');
	}
	
	function do_upload(){

        $config['upload_path']="./assets/surat_perusahaan";
        $config['allowed_types']='pdf';
        $config['encrypt_name'] = TRUE;
		$this->load->library('upload',$config);
		
			// var_dump($this->upload->do_upload("file"));
			// exit();
		
	    if($this->upload->do_upload("file")){

			$data = array('upload_data' => $this->upload->data());
			$id_perusahaan= is_id_perusahaan();
				// var_dump($id_perusahaan);
				// exit();
			$image= $data['upload_data']['file_name']; 
			$datas = array(
	        	'status_komfirmasi' => 'Waiting Verification',
	        	'surat' => $image
			   );  
			   	   
			$this->db->where('id_perusahaan',$id_perusahaan);
			$update=$this->db->update('perusahaan', $datas);
	        // $result= $this->m_upload->simpan_upload($judul,$image);
			echo json_decode($update);
			
        }else{
			// var_dump(66);
			// 	exit();

			
			$result='gagal';
			echo json_decode($result);
		}
	}
}