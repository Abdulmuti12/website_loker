<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loker extends CI_Controller {

    function __construct(){

		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_loker');
    }
    public function index()
    {
		if($this->session->userdata('nama') != NULL){
			$this->load->view('Template/header');
			$this->load->view('Template/flashdata');
			$this->load->view('loker/index');
			$this->load->view('Template/footer');
		}else{
			redirect('secure');
		}
	}

	function json_loker()
	{
		header('Content-Type: application/json');
		echo $this->M_loker->get_admin_loker();		
	}

	function json_loker_alumni()
	{
		header('Content-Type: application/json');
		echo $this->M_loker->get_alumni_loker();		
	}

	function delete(){ //function hapus data
		$id_loker=$this->input->post('id_loker');
		$this->db->where('id_loker',$id_loker);
		$delete=$this->db->delete('loker');
		if($delete == 1){
			
			succes_Delete();
		}
		else{

		}	  
		redirect('Loker');
	}

}