<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelengkapan_data extends CI_Controller {

    function index(){

        $id_alumni= is_id_alumni();
        $query=$this->db->query(" SELECT resume.id_alumni as id_alumni,cv,ijasah,transkip_nilai,lain2 from alumni INNER JOIN resume ON resume.id_alumni=alumni.id_alumni where alumni.id_alumni='$id_alumni' ");
        // get all variable
        $data['resume']=$query->row_array();
        // hitung
        $count=$query->num_rows();
        // $count1=$data['resume']['id'];
        // send variable to controller
        $this->session->set_flashdata('resume', $data['resume']);


        if($count <= 0){
            $data['text']="Lengkapi data terlebih dahulu";
            $data['verifikasi']="<button class='btn btn-info center upload'>Upload</button> ";
        }
        else{
            $data['text']="";
            $data['verifikasi']="";

        }

        $this->load->view('Template/header_alumni');
		$this->load->view('Template/flashdata');
		$this->load->view('alumni_page/kelengkapan_data/index',$data);
		$this->load->view('Template/footer');
	
    }

    function upload(){

        $config['upload_path']="./assets/data_alumni";
        $config['allowed_types']='gif|jpg|png|pdf';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);

        if($this->upload->do_upload("cv")){
			$data = array('upload_data' => $this->upload->data());
			$cv= $data['upload_data']['file_name']; 
        }   
        if($this->upload->do_upload("ijasah")){
			$data = array('upload_data' => $this->upload->data());
			$ijasah= $data['upload_data']['file_name']; 
        } 
        if($this->upload->do_upload("transkip_nilai")){
			$data = array('upload_data' => $this->upload->data());
			$transkip_nilai= $data['upload_data']['file_name']; 
        } 
        if($this->upload->do_upload("lain2")){
			$data = array('upload_data' => $this->upload->data());
			$lain2= $data['upload_data']['file_name']; 
        } 
        
        $id_alumni= is_id_alumni();
        $record= array(
                'id_alumni'=>$id_alumni,'cv'=>@$cv,'ijasah'=>@$ijasah,
                'transkip_nilai'=>@$transkip_nilai,'lain2'=>@$lain2
        );  
        
        $excute=$this->db->insert('resume', $record);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
    }

    function upload_edit(){
        
        $config['upload_path']="./assets/data_alumni";
        $config['allowed_types']='gif|jpg|png|pdf';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        $field=$_POST['field'];
        
        // hapus_file
        $nama_sblm = $this->input->post('nama_sblm');
        $ac=unlink('./assets/data_alumni/'.$nama_sblm);
        // var_dump($ac);
        // exit();
        
        if($this->upload->do_upload("file")){
			$data = array('upload_data' => $this->upload->data());
			$file= $data['upload_data']['file_name']; 
        }   

        $record=array(
            $field => $file  
        );


        $id_alumni= is_id_alumni();

		$this->db->where('id_alumni',$id_alumni);
        $update=$this->db->update('resume', $record);
        if($update == 1){
			
            succes_Update();
            $status='sukses';
            echo json_encode($status,TRUE);

            // return
		};
        

    }    

    function load_form_upload(){

        return $this->load->view('alumni_page/kelengkapan_data/form_upload');
    }

    public function load_resume(){
        // get from send controller
        $col['resume1'] = $this->session->flashdata('resume');
        return $this->load->view('alumni_page/kelengkapan_data/form_resume',$col);
    }
}
