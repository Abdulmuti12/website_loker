<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_admin');
	}

	public function index()
    {
		$data['page_name']='Page Admin';
		$data['status']=array("Aktif","Non Aktif");
		if($this->session->userdata('nama') != NULL){
			
			$this->load->view('template/header',$data);
			$this->load->view('Template/flashdata');
			$this->load->view('admin/index');
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure'));
		}
	}

	public function preview_admin()
	{
		$id=$this->input->post('id');
		$data=$this->db->get_where('admin',['id_admin'=>$id])->row_array();
		echo json_encode($data,TRUE);
	}

	function save(){ //function simpan data
		$data=array(
		  'nama'     => $this->input->post('nama'),
		  'username' => $this->input->post('username'),
		  'email'    => $this->input->post('email'),
		  'status'   => $this->input->post('status'),
		  'password' => md5(12345)
		);

		$excute=$this->db->insert('admin', $data);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
		redirect('admin');
	}

	function update(){ //function update data

		$id_admin=$this->input->post('id_admin');
		$data=array(
		  'nama'=> $this->input->post('nama'),
		  'username'=> $this->input->post('username'),
		  'email' => $this->input->post('email'),
		  'status' => $this->input->post('status')
		);
		$this->db->where('id_admin',$id_admin);
		$update=$this->db->update('admin', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('admin');
	}

	function delete(){ //function hapus data
		$id_admin=$this->input->post('id_admin');
		$this->db->where('id_admin',$id_admin);
		$this->db->delete('admin');
		redirect('admin');
	}

	function json_admin()
	{
		header('Content-Type: application/json');
		echo $this->M_admin->get_all_admin();		
	}
	

	function data_admin()
	{
		$dataa=$this->M_admin->user_list();
		echo json_encode($dataa);
	}

	function detail_admin()
	{
		// $data['page_name']='Detail Admin';
		// $data['status']=array("Aktif","Non Aktif");
		if($this->session->userdata('nama') != NULL){
			
			$this->load->view('template/header');
			$this->load->view('Template/flashdata');
			$this->load->view('admin/detail_admin');
			$this->load->view('template/footer');
		}
		else{
			redirect(base_url('secure'));
		}
	}

	function status(){

		$a=$this->input->post('id');
		$b=$this->db->get_where('admin',['id_admin'=>$a])->row_array();
		
		if($b['status'] == 'Aktif'){
			$data =["status" => "Non Aktif"];
		}
		else{
			$data =["status" => "Aktif"];
		}

		$this->db->where('id_admin',$b['id_admin']);
		$this->db->update('admin',$data);
		return  "succes";
		
	}

	function do_upload(){

        $config['upload_path']="./assets/images";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
		$this->load->library('upload',$config);
		
	    if($this->upload->do_upload("file")){

			$data = array('upload_data' => $this->upload->data());
			$id_admin= is_id();
			$image= $data['upload_data']['file_name']; 
			$datas = array(
	        	'update' => 3,
	        	'gambar' => $image
			   );  
			   	   
			$this->db->where('id_admin',$id_admin);
			$update=$this->db->update('admin', $datas);

	        // $result= $this->m_upload->simpan_upload($judul,$image);
			echo json_decode($update);
			
        }else{
			
			$result='gagal';
			echo json_decode($result);
		}
	}
	
}