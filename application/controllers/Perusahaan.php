<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

    function __construct(){

		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('M_perusahaan');
    }
    
    public function index()
    {
		// $x['jurusan']=$this->M_alumni->get_jurusan();
		$data['title']="Page Perusahaan";
		if($this->session->userdata('nama') != NULL){

			$this->load->view('Template/header');
			$this->load->view('Template/flashdata');
			$this->load->view('perusahaan/index',$data);
			$this->load->view('Template/footer');
			
		}else{
			redirect(base_url('secure'));
		}
    }

	function create(){ //function simpan data
		$data=array(
		  'nama_perusahaan' => $this->input->post('nama'),
		  'alamat_perusahaan' => $this->input->post('alamat'),
		  'telp_perusahaan'=> $this->input->post('telp'),
		  'alamat_perusahaan'=> $this->input->post('alamat'),
		  'keterangan_perusahaan'=> $this->input->post('keterangan'),
		  'perwakilan_perusahaan'=> $this->input->post('perwakilan'),
		  'no_tlpn'=> $this->input->post('no_tlpn'),
		  'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
		  'tahun_angkatan'=> $this->input->post('tahun_angkatan')
		);
		// var_dump($data);
		// exit();	
		$excute=$this->db->insert('alumni', $data);
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	
		redirect('alumni/index');
	}

	public function preview()
	{
		$id=$this->input->post('id');
		$data=$this->db->get_where('perusahaan',['id_perusahaan'=>$id])->row_array();
		echo json_encode($data,TRUE);
	}

	function update(){ //function update data

		$id_alumni=$this->input->post('id_alumni');
		$data=array(
			'nama_perusahaan' => $this->input->post('nama_perusahaa'),
			'alamat_perusahaan' => $this->input->post('alamat_perusahaa'),
			'telp_perusahaan'=> $this->input->post('telp'),
			'alamat_perusahaan'=> $this->input->post('alamat_perusahaa'),
			'keterangan_perusahaan'=> $this->input->post('keterangan_perusahaa'),
			'perwakilan_perusahaan'=> $this->input->post('perwakilan_perusahaa'),
			'telp_perusahaan'=> $this->input->post('telp_perusahaan')
			// 'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
			// 'tahun_angkatan'=> $this->input->post('tahun_angkatan')
		  );	
		$this->db->where('id_perusahaan',$id_perusahaan);
		$update=$this->db->update('perusahaan', $data);
		if($update == 1){
			
			succes_Update();
		}
		else{

		}	  
		redirect('perusahaan');
	}

    function delete(){ //function hapus data
		$id_perusahaan=$this->input->post('id_perusahaan');
		$this->db->where('id_perusahaan',$id_perusahaan);
		$delete=$this->db->delete('perusahaan');
		if($delete == 1){

			succes_Delete();
			
		}else{

		}
		redirect('perusahaan');
	}

    function json_perusahaan()
	{
		header('Content-Type: application/json');
		echo $this->M_perusahaan->get_all_perusahaan();
	}

	public function view_pdf(){

		$id=$this->input->post('id');
		$data=$this->db->get_where('perusahaan',['id_perusahaan'=>$id])->row_array();
		echo json_encode($data,TRUE);
		
	}

	function comfirm(){

		$id=$this->input->post('id');
		$status=$this->input->post('status');
		$data=array('id_perusahaan'=>$id,'status_komfirmasi'=>$status);		
		$this->db->where('id_perusahaan',$id);
		$update=$this->db->update('perusahaan', $data);

		return $update;
	}

	function perusahaan_loker(){

		$id=$this->input->post('id_perusahaan');
		$this->session->set_flashdata('id', $id);

		$view =$this->load->view('perusahaan/loker_perusahaan');

		return $view;
	}

	function json_perusahaan_loker() {

		$id = $this->session->flashdata('id');
        $this->datatables->select('id_loker, posisi, gaji, jenjang_pendidikan, jurusan.jurusan, pengalaman, fasilitas, tugas, jaminan, jenis_kelamin, penempatan, waktu_bekerja, dibuka, ditutup,perwakilan_perusahaan, loker.id_perusahaan');
		$this->datatables->from('loker');
		$this->datatables->join('perusahaan', 'perusahaan.id_perusahaan=loker.id_perusahaan');
		$this->datatables->join('jurusan', 'jurusan.id_jurusan=loker.jurusan');
		$this->datatables->where("perusahaan.id_perusahaan ='$id' ");
		$this->datatables->add_column('view', 
		'<a href="javascript:void(0);" class="dtl btn1" id="$1" ><i class="fa fa-bars"></i></a>
		 <a href="javascript:void(0);" class="hapus_record btn1 btn-xs" id="$1"><i class="fas fa-trash text-gray-300"></i></a>
		',
		'id_loker');
		echo $this->datatables->generate();
			
	 }
}