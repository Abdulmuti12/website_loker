<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secure extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_secure');      
	}
	
    public function home(){ 
		$this->load->view('template/home');
	}
    
    public function login(){

        $username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
		);
		$cek = $this->m_secure->cek_login("admin",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'nama' => $username,
				'status' =>'aktif');

			$this->session->set_userdata($data_session);
			redirect(base_url("template"));

		}else{
            $this->session->set_flashdata('Gagal','Email Atau Password Anda Salah');
			redirect('secure');
		}
	}    
	
    public function login_alumni()
    {
        $username = $this->input->post('nis');
		$password = $this->input->post('tanggal_lahir');
		$where = array(
			'nis' => $username,
			'tanggal_lahir' => $password
			);
		$cek = $this->m_secure->alumn_login("alumni",$where)->row_array();

		if($cek != NULL){
			$data_session = array(
				'id_alumni' => $cek['id_alumni'],
				'nama' => $cek['nama_lengkap'],
				'status' =>'Alumni');
				// var_dump($cek->id_admin);
				// exit();
			$this->session->set_userdata($data_session);
			redirect(base_url("template_alumni"));
		}else{
            $this->session->set_flashdata('Gagal','Email Atau Password Anda Salah');
			redirect('secure');
		}
	}  
	  
    public function login_perusahaan()
    {
        $username = $this->input->post('email_perusahaan');
		$password = $this->input->post('password_perusahaan');
		$password1= $this->encrypt->encode($password);
		$password2=sha1($password);
		$where = array(
			'email_perusahaan' => $username,
			'password_perusahaan' => $password2
			);
		$cek = $this->m_secure->perusahaan_login("perusahaan",$where)->row_array();
		
			// var_dump($password2);
			// exit();
		

		if($cek != NULL){
			$data_session = array(
				'id_perusahaan' => $cek['id_perusahaan'],
				'nama' => $cek['perwakilan_perusahaan'],
				'status' =>'perusahaan');

			$this->session->set_userdata($data_session);

			redirect(base_url("P_data_perusahaan"));
		}else{
            $this->session->set_flashdata('Gagal','Email Atau Password Anda Salah');
			redirect('secure/perusahaan_login');
		}
    }    

    public function index()
    {
        $this->load->view('login');
	}
	
    public function alumn()
    {
        $this->load->view('login_alumni');
	}
	
    public function registrasi_perusahaan()
    {
		$this->form_validation->set_rules('perwakilan_perusahaan','Perwakilan Perusahaan','required');
		$this->form_validation->set_rules('nama_perusahaan','Nama Perusahaan','required');
		$this->form_validation->set_rules('alamat_perusahaan','Alamat Perusahaan','required');
		$this->form_validation->set_rules('email_perusahaan','Email perusahaan','required');
		$this->form_validation->set_rules('telp_perusahaan','Telephone Perusahaan','required');
		$generator_code=$this->encrypt->encode($this->input->post('perwakilan_perusahaan').date('m'));
		$code=substr($generator_code,1-15);
		$email=$this->input->post('email_perusahaan');
		$data=array(
			'perwakilan_perusahaan' => $this->input->post('perwakilan_perusahaan'),
			'nama_perusahaan' => $this->input->post('nama_perusahaan'),
			'alamat_perusahaan'    => $this->input->post('alamat_perusahaan'),
			'email_perusahaan'   => $email,
			'telp_perusahaan' =>  $this->input->post('telp_perusahaan'),
			'security_code' =>  $code
		);

		$excute=$this->db->insert('perusahaan', $data);
	
		if($excute == 1){
			
			succes_add();
		}
		else{

		}	

		$this->send_mail($code,$email);
		
		redirect('secure/home/');
	}
	
    public function perusahaan_login()
    {
    	$this->load->view('login_perusahaan');
    }
    public function perusahaan_config()
    {
		$this->load->view('perusahaan_page');
	}

	function send_mail($code,$email){
		$config = array(
				'protocol' => 'smtp', // 'mail', 'sendmail', or 'smtp'
				'smtp_host' => 'ssl://smtp.googlemail.com', 
				'smtp_port' => 465,
				'smtp_user' => 'abdulmfmuti@gmail.com',
				'smtp_pass' => 'Abdulmuti_',
				// 'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
				'mailtype' => 'html', //plaintext 'text' mails or 'html'
				// 'smtp_timeout' => '4', //in seconds
				'charset' => 'iso-8859-1'
				// 'wordwrap' => TRUE
			);

		$this->load->library('email',$config);
		$this->email->initialize($config);// add this line

		$this->email->set_newline("\r\n");
		$this->email->from('abdulmu0809@bsi.ac.id','kagami');
		$this->email->to($email);
		$this->email->subject('test');
		$this->email->message(base_url('secure/comfirm_password/').$code);
		$this->email->send();
		// echo $this->email->print_debugger();
	}
	
	public function comfirm_password ($code = NULL){
		$data['code']=$code;
		$this->load->view('template/comfirm_password',$data);	
	}
	public function remacth_password(){
		$code=$this->input->post('code');
		$password1=$this->input->post('password1');
		$password2=$this->input->post('password2');
		$password3= $this->encrypt->encode($password1);
		
		$password =["password_perusahaan" => sha1($password1)];

		if($password1 == $password2){
			$this->db->where('security_code',$code);

				$update=$this->db->update('perusahaan', $password);
					echo $this->db->last_query();
				if($update == 1){
					
					succes_Update();
				}
				else{

				}	  			
		}
		$this->session->set_flashdata('login_on','Email Atau Password Anda Salah');
		redirect('secure/perusahaan_login');	
		
	}

    function logout(){
		$this->session->sess_destroy();
		redirect(base_url('secure/home'));
	}
}