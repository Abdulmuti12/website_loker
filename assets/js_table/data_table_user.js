$(document).ready(function(){
  	    
    tampil_data_user(); //pemanggilan fungsi user	    
  	$('#datauser').dataTable();
	
    function tampil_data_user(){
	        $.ajax({
	            type  : 'ajax',
	            url   : '<?php echo base_url()?>user/data_user',
	            async : false,
	            dataType : 'json',
	            success : function(data){
	                var html = '';
	                var i;
	    
	                for(i=0; i<data.length; i++){
	                    html += '<tr>'+
	                          '<td>'+data[i].nama+'</td>'+
	                            '<td>'+data[i].email+'</td>'+
	                            '<td>'+data[i].tlpn+'</td>'+
	                            '<td>'+"<button class='btn btn-primary status' id="+data[i].id_user+">ubah</button>"+data[i].status+'</td>'+
	                            '<td>'+'<a href='+'<?=base_url('user/detail_user');?>/'+data[i].id_user+' '
	                                    +'class='+'btn'+'btn-primary '+
	                                    ''+'><i class="fas fa-clipboard-list fa-1x text-gray-300"></i> Detail </a>'+
	                                    '<a href='+'<?=base_url('user/ubah_user');?>/'+data[i].id_user+' '+
	                                    'class='+'btn'+'btn-primary '+''+'><i class="fas fa-exclamation-triangle text-gray-300"></i> Edit </a>'+

	                            '<a href='+'<?=base_url('user/hapus_user');?>/'+data[i].id_user+' '+'class='+'btn'+'btn-primary '+''+'><i class="fas fa-trash text-gray-300"></i>  Hapus</a>'+
	                            '</td>'+
	                            '</tr>';
                }
                $('#show_user').html(html);
            }
	   });
	}
        
    $(".status").click(function() {
    // alert(this.id);
        var id=this.id;
        var status=this.status;
        postdata={id:id,status:status};
        $.ajax({
        method:'POST',
            url:'<?php echo base_url()?>user/status',
            data:postdata,
            success: function(response) { 
                // refresh
                setTimeout(function(){
                    location.reload(); 
                 }, 1000); 
            }
        });                 
    });    

});
